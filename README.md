Adzuna perl test repository

This is a git repository with a Vagrantfile and a small Catalyst application
(named "Jobs").

NOTES FOR TAKING THE TEST REMOTELY
==================================

This test should take around an hour, maybe a little bit more. We originally
created it to be taken in our office. We don't want to take lots of your time.

Running the test Virtual Machine
--------------------------------

This test contains a Vagrant file for managing a test VM. The VM has all
the dependencies needed to run this test included.

You will need:

- VirtualBox https://www.virtualbox.org/wiki/Downloads
- Vagrant https://www.vagrantup.com/downloads.html

Please setup the VM before trying to complete the tasks.

---

The aims of the test are explained below under "OVERVIEW OF THE TASK".

The second part of the task is open-ended but we are more interested in
how you prioritise your time, than something fancy, so please don't
spend lots of time. Many people taking the test in the office provide
just a list of what they would do next.

If anything is not working please do contact us. There is not meant to be
any debugging or deliberate confusion introduced to this test.

As well as reading this file, we encourage you to look at the git history
to see what we have done to build the application.

We would also encourage you to commit in stages, especially separating part
one and part two, with meaningful commit messages.

STARTING THE VM
===============

In this directory there is a Vagrant file for controlling the VM. With vagrant
installed, `cd` into this directory:

- Run `vagrant up` to start up the VM.

After provisioning has completed and the VM has started:

- Run `vagrant ssh` to connect to the VM.

If you want to see how the machine was provisioned, look in the Vagrantfile.
The provision steps are a simple shell script.

MODIFYING THE APPLICATION
=========================

The whole of this directory will be shared with the VM, so you can edit files
on the host machine or in the VM. The shared directory will appear as `/vagrant`
inside the VM.

RUNNING THE APPLICATION
=======================

You must be inside the VM to run the application, helper scripts, or connect
to the database. Use `vagrant ssh` within this repository on the host to
connect to the VM.

The application is in the Jobs subdirectory. So, inside the VM cd to
/vagrant/Jobs to get started.

The application is a basic catalyst application. Start a test server by
running script/jobs_server.pl from the /vagrant/Jobs directory.

    $ cd /vagrant/Jobs && perl script/jobs_server.pl

There are other scripts in the script directory, some created by catalyst and
some added by us, which you may find helpful.

There is a small MySQL database on the VM. You can connect via the MySQL command
line client:

    $ mysql -uroot -prootpass adztest_jobs

TESTING THE APPLICATION
=======================

The test server will start on port 3000. The Vagrantfile shares the VM port 3000
with the host machine, also on port 3000, so a web browser on the host going to
http://localhost:3000/ should see the test application.

Before making any modifications, check that the home page shows a list of 3
canned jobs, that you can post another job using the Post a Job Ad link, and
check it appears on the home page.

If you make changes to a Perl file, you will need to stop and restart the test
server (Ctrl-C). TT (template toolkit) changes should automatically load without
a restart.

OVERVIEW OF THE TASK
====================

We have a specific primary goal, plus optional extras. We really want to see
working code for the primary goal. We would prefer to see something imperfect
working for the primary goal, than an non-functional implementation with
advanced features (e.g. validation, number formatting etc).

The secondary goal is open-ended, and you should spend as much or as little time
as you have after completing the primary goal. If you decide to change code
for the secondary goal, we suggest making use of git to isolate your working
primary code from the later changes. We also suggest using git to see the
steps we took to create the application so far, as this may help you.

Using books, searches and asking questions are encouraged.

PRIMARY GOAL
------------

We are iteratively building a new job board product that we would like to launch
soon. Currently it has a way to list jobs and post new jobs.

The next prioritised feature is to include a salary field, so that we can
show that separately and potentially sort/filter jobs by salary.

For this step, please add a salary field to the database, the job posting form
and the list of "latest" jobs, so that by the end we can post a job with salary
data and see the salary clearly in the latest jobs list.

There are already some jobs without any salary data, so the latest jobs list
will have to continue to deal with them.

SECONDARY GOAL
--------------

Only attempt after you have something working for the primary goal, and commit
it to git so we can go back to it.

What would you do as the next priority for this application?

For example:

* give us a short list of priorities (up to 3)
* choose one idea and implement it
* give us a list of priorities and implement the easiest

We are more interested in what you choose to do any why, rather then having
something working for this. There is not one particular answer that we are
looking for here.
