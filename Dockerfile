FROM perl

RUN cpanm -qn Carton && \
    mkdir -p /usr/src/app/bin

WORKDIR /usr/src/app

ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.7.3/wait bin/wait
RUN chmod +x bin/wait

COPY ./Jobs/cpanfile /usr/src/app
RUN carton install && sed -i \
  's|Catalyst::Controller::RequestToken::Action|Catalyst::Action::RequestToken|g' \
  ./local/lib/perl5/Catalyst/Controller/RequestToken/Action/*.pm \
  && sed -i \
  's|Catalyst::Controller::RequestToken::Action|RequestToken|g' \
  ./local/lib/perl5/Catalyst/Controller/RequestToken.pm \
  && mkdir -p ./local/lib/perl5/Catalyst/Action/RequestToken \
  && mv ./local/lib/perl5/Catalyst/Controller/RequestToken/Action/*.pm \
  ./local/lib/perl5/Catalyst/Action/RequestToken/

COPY ./Jobs /usr/src/app
COPY ./docker_start.sh /usr/src/app/bin/

ENV PATH="./bin:./local/bin:${PATH}" \
  PERL5LIB="./local/lib/perl5:./lib"

EXPOSE 80

CMD bin/wait && docker_start.sh
