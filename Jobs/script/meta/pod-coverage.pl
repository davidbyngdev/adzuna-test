#!perl
use strict;
use warnings;
use v5.18.2;
use Test::Pod::Coverage;
use Pod::Coverage;
use List::Util qw/ sum /;
use lib '/vagrant/Jobs/lib';

my @modules = Test::Pod::Coverage::all_modules();

$ENV{CATALYST_DEBUG} = 0;

my @coverage;
foreach my $module ( @modules ) {
    my $pc = Pod::Coverage->new(package => $module);
    if ( my $coverage = $pc->coverage ) {
	    say "Coverage for $module: " . $coverage;
	    push @coverage, $coverage;
    }
    else {
	say "No coverage for $module: ". $pc->why_unrated;
	if ( $pc->why_unrated eq q{couldn't find pod} ) {
		push @coverage, 0;
	}
    }
}

my $coverage = sum(@coverage);
my $coverage_percentage = ( $coverage / scalar @coverage ) * 100; 
say "------------------";
say "POD Coverage: " . sprintf('%2d',$coverage_percentage) . '%';

exit(0);
