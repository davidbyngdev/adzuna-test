#!perl
use strict;
use warnings;

$ENV{CATALYST_DEBUG} = 0;

system('cover -delete');
system('HARNESS_PERL_SWITCHES=-MDevel::Cover=+ignore,\\.t$ prove -lr t/');
system('cover -summary');

exit(0);
