#!/usr/bin/env perl
use lib './lib';
use Jobs::Model::JobsDB;
use utf8;

my $db = Jobs::Model::JobsDB->new->schema;

my $rs = $db->resultset('Job');

$rs->create({
    title => 'Bar tender',
    description => 'Provide customer delight by exchanging money for drinks',
});

$rs->create({
    title => 'Wait staff',
    description => 'Excellent customer service blah blah blah',
});

$rs->create({
    title => 'Chef',
    description => 'Prepare a wide range of contemporary cuisine in our state of the art kitchen',
});

$rs->create({
    title => 'Non-Latin1 text job',
    description => '⊥ < a ≠ b ≡ c ≤ d ⇒ (A ⇔ B) მსოფლიოს እንደ ጉረቤቱ አይተዳደርም። ⠞ ⡊ ⠅⠝⠪⠂ ⠕⠋ ⠍⠹ ⑀₂ἠḂӥẄɐː⍎אԱა',
});
