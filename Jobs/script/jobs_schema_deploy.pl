#!/usr/bin/env perl
use lib './lib';
use Jobs::Model::JobsDB;

my $db = Jobs::Model::JobsDB->new->schema;
$db->deploy({add_drop_table => 1});
