use strict;
use warnings;
use Test::More;


use Catalyst::Test 'Jobs';
use Jobs::Controller::Ads;

ok( request('/ads')->is_success, 'Request should succeed' );
done_testing();
