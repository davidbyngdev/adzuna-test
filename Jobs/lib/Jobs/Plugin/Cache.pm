package Jobs::Plugin::Cache;

use strict;
use warnings;
use Exporter 'import';

our @EXPORT = qw(no_cache);
our $VERSION = '0.1';

sub no_cache {
    my ($c) = @_;

    $c->response->headers->header('Cache-Control' => 'no-store, no-cache, must-revalidate');
    $c->response->headers->header('Expires' => -1);
    $c->response->headers->header('Pragma' => 'no-cache');
}

=head1 NAME

Jobs::Plugin::Cache - Set no-cache headers for Catalyst routes

=head1 DESCRIPTION

Set the default headers required for web browser to prevent caching the response

=head1 SYNOPSIS

Set non-cache headers on default Catalyst route

  use Jobs::Plugin::Cache;

  sub default :Path () {
      my ($self, $c) = @_;
      no_cache($c);
      $c->reponse->body('done');
  }

=head1 METHODS

=over 1

=item no_cache()

Set cache control headers to prevent browser caching response

=back

=head1 AUTHOR

David Byng, C<< <davidbyng at gmail.com> >>

=head1 LICENSE AND COPYRIGHT

Copyright 2021-01 David Byng.

This program is free software; you can redistribute it and/or modify it
under the terms of either: the GNU General Public License as published
by the Free Software Foundation; or the Artistic License.

See http://dev.perl.org/licenses/ for more information.

=cut

1;
