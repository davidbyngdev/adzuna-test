package Jobs::View::Web;
use Moose;
use namespace::autoclean;

extends 'Catalyst::View::TT';

__PACKAGE__->config(
    # Change default TT extension
    TEMPLATE_EXTENSION => '.tt',
    render_die => 1,
    # Set the location for TT files
    INCLUDE_PATH => [
            Jobs->path_to( 'root', 'src' ),
        ],
    # This is your wrapper template located in the 'root/src'
    WRAPPER => 'wrapper.tt',
);

=head1 NAME

Jobs::View::Web - TT View for Jobs

=head1 DESCRIPTION

TT View for Jobs.

=head1 SEE ALSO

L<Jobs>

=head1 AUTHOR

vagrant,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
