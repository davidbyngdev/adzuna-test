package Jobs::Controller::Ads;
use Moose;
use namespace::autoclean;
use Jobs::Plugin::Cache;

BEGIN { extends 'Catalyst::Controller::RequestToken'; }

=head1 NAME

Jobs::Controller::Ads - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index

=cut

=head2 form_create

Form for posting a new Job Ad

=cut

sub form_create :Path('form_create') :Args(0) {
    my ($self, $c) = @_;

    no_cache($c);

    $c->stash(
        request_token => $self->create_token($c),
        template => 'ads/form_create.tt');
}

=head2 form_create_do

Take information from form and add to database

=cut

sub form_create_do :Path('form_create_do') :Args(0) {
    my ($self, $c) = @_;

    no_cache($c);

    # Validate CSRF token
    return unless $self->validate_csrf($c);

    # Retrieve the values from the form
    my $title       = $c->request->params->{title}       || 'N/A';
    my $description = $c->request->params->{description} || 'N/A';
    my $salary      = (
        $c->request->params->{salary}
        && $c->request->params->{salary} =~ m/^[\d\.]+$/ ?
        $c->request->params->{salary} : undef
    );

    # Create the advert
    my $job_ad = $c->model('JobsDB::Job')->create({
            title   => $title,
            description => $description,
            salary => $salary
        });

    # Store new model object in stash and set template
    $c->stash(
        ad => $job_ad,
        template => 'ads/create_done.tt');
}

=head2 form_update

Form for amending an existing Job Ad

=cut

sub form_update :Path('form_update') :Args(1) {
    my ($self, $c, $job_id) = @_;

    no_cache($c);

    $c->stash(
        ad => $c->model('JobsDB::Job')->find($job_id),
        request_token => $self->create_token($c),
        template => 'ads/form_update.tt');
}

=head2 form_update_do

Take information from form and update database

=cut

sub form_update_do :Path('form_update_do') :Args(0) {
    my ($self, $c) = @_;

    no_cache($c);

    # Validate CSRF token
    return unless $self->validate_csrf($c);

    # Retrieve the values from the form
    my $title       = $c->request->params->{title};
    my $description = $c->request->params->{description};
    my $salary      = $c->request->params->{salary}      || undef;

    # Update the job advert
    my $job_ad = $c->model('JobsDB::Job')->find($c->request->param('id'));

    if ($job_ad) {
        $job_ad->title($title) if $title;
        $job_ad->description($description) if $description;
        $job_ad->salary($salary) if $salary =~ m/^[\d\.]+$/;
        $job_ad->update;
    }

    # Store new model object in stash and set template
    $c->stash(
        ad => $job_ad,
        template => 'ads/update_done.tt');
}

=head2 delete

Purge an existing Job Ad

=cut

sub delete :Path('delete') :Args(2) {
    my ($self, $c, $job_id, $_token) = @_;

    no_cache($c);

    # Validate CSRF token
    $c->request->param('_token' => $_token);
    return unless $self->validate_csrf($c);

    if (my $job_ad = $c->model('JobsDB::Job')->find($job_id)) {
        $c->stash(
            ad => {id => $job_ad->id, title => $job_ad->title},
            template => 'ads/delete_done.tt');
        $job_ad->delete;
    }

    $c->stash(template => 'ads/delete_done.tt');
}

=head2 validate_csrf

Validate CSRF token

=cut

sub validate_csrf {
    my ($self, $c) = @_;

    unless ($self->validate_token($c)) {
        $c->log->warn(sprintf('Invalid CSRF token %s', $c->request->path));
        $c->response->status(400);
        $c->response->body('Invalid operation.');
        return;
    }

    return 1;
}

# Ensure CSRF ident is unified between classes
sub _ident {
    return '__Jobs::Controller_token';
}

=encoding utf8

=head1 AUTHOR

David Byng, C<< <davidbyng at gmail.com> >>

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
