package Jobs::Controller::Root;
use Moose;
use namespace::autoclean;
use Locale::Currency::Format;
use Jobs::Plugin::Cache;

BEGIN { extends 'Catalyst::Controller::RequestToken' }

#
# Sets the actions in this controller to be registered with no prefix
# so they function identically to actions created in MyApp.pm
#
__PACKAGE__->config(namespace => '');

=encoding utf-8

=head1 NAME

Jobs::Controller::Root - Root Controller for Jobs

=head1 DESCRIPTION

[enter your description here]

=head1 METHODS

=head2 index

The root page (/)

=cut

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;

    no_cache($c);

    my $jobs = [];
    my $query = $c->request->param('query');
    my $search_params = {
        result_class => 'DBIx::Class::ResultClass::HashRefInflator',
        row => 10
    };

    # Search jobs by user string
    if ($query) {
        # Text columns
        my @search_filter = qw(title description);
        my @bind = ($query, $query);

        # Salary column
        if ($query =~ m/^[\d\.]+$/) {
            push @search_filter, 'salary';
            push @bind, $query;
        }

        # Format like values
        @bind = map{s/%/%%/g; sprintf('%%%s%%', $_)} @bind;

        $jobs = [
            $c->model('JobsDB')
              ->resultset('Job')
              ->search_literal(
                  join(' OR ', map{sprintf('me.%s LIKE ?', $_)} @search_filter),
                  @bind,
                  $search_params
              )->all
        ];
    }
    # All jobs
    else {
        $jobs = [
            $c->model('JobsDB')
              ->resultset('Job')
              ->search({}, $search_params)->all
        ];
    };

    $c->stash(
        query => $query,
        latest_jobs => $jobs,
        currency => sub {currency_format('GBP', shift, FMT_HTML)},
        category => 'Test',
        request_token => $self->create_token($c),
        template => 'index.tt');
}

=head2 default

Standard 404 error page

=cut

sub default :Path {
    my ( $self, $c ) = @_;
    $c->response->body( 'Page not found' );
    $c->response->status(404);
}

# Ensure CSRF ident is unified between classes
sub _ident {
    return '__Jobs::Controller_token';
}

=head2 end

Attempt to render a view, if needed.

=cut

sub end : ActionClass('RenderView') {}

=head1 AUTHOR

David Byng, C<< <davidbyng at gmail.com> >>

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
