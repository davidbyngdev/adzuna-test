use strict;
use warnings;

use Jobs;

my $app = Jobs->apply_default_middlewares(Jobs->psgi_app);
$app;

