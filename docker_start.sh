#!/bin/bash

# Create RDBMS schema
perl ./script/jobs_schema_deploy.pl

# Seed schema
perl ./script/jobs_add_test_data.pl

# Start starman server
plackup -s Starman --workers=10 \
  -p 80 -a ./jobs.psgi
